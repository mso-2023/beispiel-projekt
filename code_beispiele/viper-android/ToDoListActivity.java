public interface ToDoListView {
    void showToDoItems(List<String> items);
}

public class ToDoListActivity extends AppCompatActivity implements ToDoListView {
    private ToDoListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_list);

        presenter = new ToDoListPresenter(this, new ToDoListInteractor());
        presenter.onViewCreated();

        // Set up UI and handle user interactions
    }

    @Override
    public void showToDoItems(List<String> items) {
        // Update the UI to display the to-do items
    }
}
