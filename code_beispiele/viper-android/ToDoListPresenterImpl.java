public interface ToDoListPresenter {
    void onViewCreated();
}

public class ToDoListPresenterImpl implements ToDoListPresenter {
    private ToDoListView view;
    private ToDoListInteractor interactor;

    public ToDoListPresenterImpl(ToDoListView view, ToDoListInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void onViewCreated() {
        List<String> toDoItems = interactor.fetchToDoItems();
        view.showToDoItems(toDoItems);
    }
}
