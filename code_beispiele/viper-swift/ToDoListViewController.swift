import UIKit

protocol ToDoListView: class {
    func showToDoItems(_ items: [String])
}

class ToDoListViewController: UIViewController, ToDoListView {
    // Outlets, actions, and other UI-related code

    var presenter: ToDoListPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
    }

    // Implement ToDoListView protocol methods
    func showToDoItems(_ items: [String]) {
        // Update the UI to display the to-do items
    }
}

