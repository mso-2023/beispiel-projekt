protocol ToDoListPresenter {
    func viewDidLoad()
}

class ToDoListPresenterImpl: ToDoListPresenter {
    weak var view: ToDoListView?
    var interactor: ToDoListInteractor!

    init(view: ToDoListView, interactor: ToDoListInteractor) {
        self.view = view
        self.interactor = interactor
    }

    func viewDidLoad() {
        let toDoItems = interactor.fetchToDoItems()
        view?.showToDoItems(toDoItems)
    }
}
