protocol ToDoListRouter {
    // Define navigation methods
}

class ToDoListRouterImpl: ToDoListRouter {
    weak var viewController: UIViewController?

    init(viewController: UIViewController) {
        self.viewController = viewController
    }

    // Implement navigation methods
}
