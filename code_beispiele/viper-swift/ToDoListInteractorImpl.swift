protocol ToDoListInteractor {
    func fetchToDoItems() -> [String]
}

class ToDoListInteractorImpl: ToDoListInteractor {
    var entity: ToDoListEntity!

    init(entity: ToDoListEntity) {
        self.entity = entity
    }

    func fetchToDoItems() -> [String] {
        // Fetch and process to-do items from the entity
        return entity.fetchToDoItems()
    }
}
