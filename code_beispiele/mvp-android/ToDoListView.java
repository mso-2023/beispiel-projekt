public interface ToDoListView {
    void showToDoItems(List<ToDoItem> items);
}
