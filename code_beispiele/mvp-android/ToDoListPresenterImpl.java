public interface ToDoListPresenter {
    void onViewCreated();

    void onAddButtonClicked(String newItemText);
}
public class ToDoListPresenterImpl implements ToDoListPresenter {
    private ToDoListView view;
    private List<ToDoItem> toDoItems = new ArrayList<>();

    public ToDoListPresenterImpl(ToDoListView view) {
        this.view = view;
    }

    @Override
    public void onViewCreated() {
        // Initial setup, fetch data, etc.
        view.showToDoItems(toDoItems);
    }

    @Override
    public void onAddButtonClicked(String newItemText) {
        ToDoItem newItem = new ToDoItem(newItemText);
        toDoItems.add(newItem);
        view.showToDoItems(toDoItems);
    }
}