import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

public class ToDoListActivity extends AppCompatActivity {
    private ToDoListViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_list);

        // Initialize ViewModel
        viewModel = new ViewModelProvider(this).get(ToDoListViewModel.class);

        // Set up RecyclerView and Adapter
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        final ToDoListAdapter adapter = new ToDoListAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        // Observe changes in the ViewModel's data
        viewModel.getToDoItems().observe(this, toDoItems -> {
            // Update the UI with the new data
            adapter.submitList(toDoItems);
        });
    }
}
