import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;
import java.util.List;

public class ToDoListActivity extends AppCompatActivity {
    private List<ToDoItem> toDoItems = new ArrayList<>();
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_list);

        // Set up UI components
        EditText newItemEditText = findViewById(R.id.newItemEditText);
        Button addButton = findViewById(R.id.addButton);
        ListView listView = findViewById(R.id.listView);

        // Set up adapter for the ListView
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new ArrayList<>());
        listView.setAdapter(adapter);

        // Set up click listener for the Add button
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newItemText = newItemEditText.getText().toString();
                addToDoItem(newItemText);
            }
        });
    }

    private void addToDoItem(String newItemText) {
        ToDoItem newItem = new ToDoItem(newItemText);
        toDoItems.add(newItem);
        updateUI();
    }

    private void updateUI() {
        List<String> itemTitles = new ArrayList<>();
        for (ToDoItem item : toDoItems) {
            itemTitles.add(item.getTitle());
        }
        adapter.clear();
        adapter.addAll(itemTitles);
    }
}
