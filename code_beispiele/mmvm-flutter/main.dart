import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'todo_model.dart';
import 'todo_view_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => ToDoListViewModel(),
      child: MaterialApp(
        title: 'To-Do List',
        home: ToDoListScreen(),
      ),
    );
  }
}

class ToDoListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('To-Do List'),
      ),
      body: ToDoList(),
      floatingActionButton: AddButton(),
    );
  }
}

class ToDoList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var toDoListViewModel = Provider.of<ToDoListViewModel>(context);
    var toDoItems = toDoListViewModel.toDoItems;

    return ListView.builder(
      itemCount: toDoItems.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(toDoItems[index].title),
        );
      },
    );
  }
}

class AddButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        // Show a dialog to get the new to-do item text
        showDialog(
          context: context,
          builder: (context) {
            var newToDoItemController = TextEditingController();
            return AlertDialog(
              title: Text('Add New To-Do Item'),
              content: TextField(
                controller: newToDoItemController,
              ),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Cancel'),
                ),
                TextButton(
                  onPressed: () {
                    var toDoListViewModel =
                        Provider.of<ToDoListViewModel>(context, listen: false);
                    toDoListViewModel.addNewItem(newToDoItemController.text);
                    Navigator.of(context).pop();
                  },
                  child: Text('Add'),
                ),
              ],
            );
          },
        );
      },
      child: Icon(Icons.add),
    );
  }
}
