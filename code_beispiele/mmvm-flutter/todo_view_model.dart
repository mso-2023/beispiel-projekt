import 'package:flutter/material.dart';
import 'todo_model.dart';

class ToDoListViewModel extends ChangeNotifier {
  List<ToDoItem> _toDoItems = [];

  List<ToDoItem> get toDoItems => _toDoItems;

  void addNewItem(String newItemText) {
    ToDoItem newItem = ToDoItem(newItemText);
    _toDoItems.add(newItem);
    notifyListeners();
  }
}
